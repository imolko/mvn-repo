#!/usr/bin/env bash

mvn -s settings.xml dependency:get -Dartifact=$1:pom  -Dtransitive=false \
    && mvn -s settings.xml dependency:get -Dartifact=$1:jar  -Dtransitive=false \
    && mvn -s settings.xml dependency:get -Dartifact=$1:jar:sources  -Dtransitive=false
